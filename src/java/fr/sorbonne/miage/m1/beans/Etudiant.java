/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.sorbonne.miage.m1.beans;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amine
 */
@XmlRootElement
public class Etudiant {
    
    private String nom;
    
    public Etudiant() {
        
    }
    
    public Etudiant(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
}
