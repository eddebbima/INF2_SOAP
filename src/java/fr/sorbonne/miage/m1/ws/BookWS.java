/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.sorbonne.miage.m1.ws;

import fr.sorbonne.miage.m1.beans.Book;
import fr.sorbonne.miage.m1.dao.BookDAO;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;

/**
 *
 * @author Amine
 */
@WebService(serviceName = "BookWS")
public class BookWS {
    
    @WebMethod(operationName = "findBookByIsbn")
    public Book findBookByIsbn(int isbn) {
        return (new BookDAO()).findById(isbn);
    }
    
    @WebMethod(operationName = "getAllBooks")
    public List<Book> getAllBooks() {
        return (new BookDAO()).findAll();
    }
}
